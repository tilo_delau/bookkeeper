Command line
 I'm starting from scratch
Set up your local directory
Set up Git on your machine if you haven't already.
mkdir /path/to/your/project
cd /path/to/your/project
git init
git remote add origin https://tilo_delau@bitbucket.org/tilo_delau/bookkeeper.git
Create your first file, commit, and push
echo "Tilo Delau" >> contributors.txt
git add contributors.txt
git commit -m 'Initial commit with contributors'
git push -u origin master
Great job, now you're all set up! Get started coding or create a team and invite people to work with you.
 I have an existing project
Already have a Git repository on your computer? Let's push it up to Bitbucket.
cd /path/to/my/repo
git remote add origin https://tilo_delau@bitbucket.org/tilo_delau/bookkeeper.git
git push -u origin --all # pushes up the repo and its refs for the first time
git push -u origin --tags # pushes up any tags