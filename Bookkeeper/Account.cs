﻿using System;
using SQLite;

namespace Bookkeeper
{
	public class Account : IAccount
	{

		public string Name { get; set; }
		[PrimaryKey]
		public int Number { get; set; }
		public string Type { get; set; }

	}
}

