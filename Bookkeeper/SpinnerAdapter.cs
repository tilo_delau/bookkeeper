﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Views;

namespace Bookkeeper
{
	public class SpinnerAdapter : BaseAdapter<Account> ,ISpinnerAdapter
	{
		private Activity context;
		private List<Account> dataSource;
		public SpinnerAdapter (Activity activity, List<Account> data)
		{
			this.context = activity;
			this.dataSource = data;
		}

		public override long GetItemId (int position) { return position; } 

		public override Java.Lang.Object GetItem (int position)
		{
			return new JavaObjectWrapper() { obj = dataSource[position]};;
		}
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View view = convertView ?? context.LayoutInflater.Inflate (Resource.Layout.spinner_item_layout, parent, false);

			view.FindViewById<TextView> (Resource.Id.spinner_item_text1).Text = dataSource[position].Name;
			view.FindViewById<TextView> (Resource.Id.spinner_item_text2).Text = "("+dataSource[position].Number+")";

	//		Console.WriteLine ("datasource name: " + dataSource[position].Number);
			return view;
		}
		public override int Count { get { return dataSource.Count; } }

		public override Account this [int index] { get { return dataSource[index]; } }			

	}
}

