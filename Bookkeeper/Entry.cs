﻿using System;
using SQLite;
namespace Bookkeeper
{
	public class Entry : IEntry
	{
		public Entry () { }

		[PrimaryKey, AutoIncrement]

		public int 		Id 			{ get; private set; }
		public string 	Date 		{ get; set; }
		public string 	Description { get; set; }
		public int		Account 	{ get; set; }
		public int 		Type 		{ get; set; }

		public double 	TotalAmount { get; set; }
		public int 	  	TaxRate 	{ get; set; }
		public double 	TaxAmount 	{ get { return TotalAmount*TaxRate; } }
		public double 	SalesAmount { get { return TotalAmount-TaxAmount; } }
		public string 	ImageFilePath { get; set;}

	}
}

