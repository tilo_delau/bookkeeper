﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace Bookkeeper
{
	[Activity (Label = "Bookkeeper", MainLauncher = true, Icon = "@drawable/icon")]

	public class MainActivity : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.activity_main);

			Button newEventBtn 		= FindViewById<Button> (Resource.Id.new_event_button);
			Button showEventBtn 	= FindViewById<Button> (Resource.Id.show_events_button);
			Button createReportBtn 	= FindViewById<Button> (Resource.Id.create_report_button);

			newEventBtn.Click 		+= delegate { StartActivity(typeof(NewEntryActivity)); };
			showEventBtn.Click 		+= delegate { StartActivity(typeof(EntryListActivity)); };
			createReportBtn.Click 	+= delegate { Toast.MakeText(this, "Haha!", ToastLength.Long).Show(); };

		}
	}
}


