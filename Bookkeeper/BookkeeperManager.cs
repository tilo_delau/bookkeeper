﻿using System;
using System.Collections.Generic;
using SQLite;
using System.Linq;

namespace Bookkeeper
{
	public sealed class BookkeeperManager : IBookkeeperManager
	{
		private static volatile BookkeeperManager instance;
		private static object syncRoot = new Object();

		static string path = (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal)) ;
		private SQLiteConnection db = new SQLiteConnection(path+ @"\database.db");

		private BookkeeperManager()
		{

			db.CreateTable<Entry>();
			db.CreateTable<TaxRate>();
			db.CreateTable<Account>();

			if (MoneyAccounts.Count == 0) 
			{
				db.Insert(new TaxRate() { Rate = 0.25 }); // has to be a double
				db.Insert(new TaxRate() { Rate = 0.15 });
				db.Insert(new TaxRate() { Rate = 0.06 });
			
				db.Insert(new Account() { Number = 3000, Name = "Försäljning", 										Type = "income" });
				db.Insert(new Account() { Number = 3040, Name = "Försäljning av tjänster", 							Type = "income" });

				db.Insert(new Account() { Number = 5400, Name = "Förbrukningsinventarier och förbrukningsmaterial", Type = "expense" });
				db.Insert(new Account() { Number = 2013, Name = "Övriga egna uttag", 								Type = "expense" });
				db.Insert(new Account() { Number = 5900, Name = "Reklam och PR", 									Type = "expense" });

				db.Insert(new Account() { Number = 1910, Name = "Kassa", 											Type = "money" });
				db.Insert(new Account() { Number = 1930, Name = "Företagskonto", 									Type = "money" });
				db.Insert(new Account() { Number = 2018, Name = "Egna insättningar", 								Type = "money" });

				// cannot close if I want to get later
			//	db.Close();

			}
		}

		public static BookkeeperManager Instance 
		{
			get
			{ 
				if (instance == null) 
				{
					lock (syncRoot)
					{
						if (instance == null) instance = new BookkeeperManager ();
					}
				}
				return instance;
			}
		}
			
		public Account GetAccount(int number) 	{ return db.Get<Account>(number); }

		public TaxRate GetTaxRate(int id) 		{ return db.Get<TaxRate>(id); }

		public Entry GetEntry (int id) 			{ return db.Get<Entry>(id); }

		public void AddEntry (Entry e)
		{
			db.Insert(e);		
		}

		public List<Entry> Entries 
		{
			get 
			{
				return db.Table<Entry>().ToList();
			}
		}

		public List<TaxRate> TaxRates 
		{
			get 
			{
				return db.Table<TaxRate>().ToList();
			}
		}

		public List<Account> IncomeAccounts 
		{
			get 
			{
				return db.Table<Account>().Where (banan => banan.Type == "income").ToList();
			}
		}

		public List<Account> ExpenseAccounts 
		{
			get 
			{
				return db.Table<Account>().Where (foo => foo.Type == "expense").ToList();
			}
		}
		public List<Account> MoneyAccounts 
		{
			get 
			{
				return db.Table<Account>().Where (bar => bar.Type == "money").ToList();
			}
		}
			
	}
}

