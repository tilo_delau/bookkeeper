﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using AUri = Android.Net.Uri;
using AFile = Java.IO.File; 

namespace Bookkeeper
{
	[Activity (Label = "EntryItemActivity")]			
	public class EntryItemActivity : Activity
	{
		private Entry entry;
		private static BookkeeperManager manager = BookkeeperManager.Instance;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.activity_entry_item);
			entry = manager.GetEntry(Intent.GetIntExtra("entry",1));
			FindViewById<TextView> (Resource.Id.tax_show_textView).Text = manager.GetTaxRate(entry.TaxRate).Rate+"";
			FindViewById<TextView> (Resource.Id.date_show_textView).Text = entry.Date;
			FindViewById<TextView> (Resource.Id.description_show_textView).Text = entry.Description;
			FindViewById<TextView> (Resource.Id.type_show_textView).Text = manager.GetAccount(entry.Type).Number +":"+manager.GetAccount(entry.Type).Name;
			FindViewById<TextView> (Resource.Id.account_show_textView).Text = manager.GetAccount (entry.Account).Number+":"+ manager.GetAccount (entry.Account).Name;
			FindViewById<TextView> (Resource.Id.amount_show_textView).Text = entry.TotalAmount+"";

			try {
			FindViewById<ImageView> (Resource.Id.show_imageView).SetImageURI (AUri.FromFile(new AFile(entry.ImageFilePath)));
			}
			catch {
				Toast.MakeText (this, "Entry: "+entry.Id, ToastLength.Long).Show ();
			}

		}
	}
}

