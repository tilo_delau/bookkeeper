﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using Android.Provider;

using AEnvironment = Android.OS.Environment; 
using AFile        = Java.IO.File; 
using AUri         = Android.Net.Uri; 


namespace Bookkeeper
{
	[Activity (Label = "New Entry")]			
	public class NewEntryActivity : Activity
	{
		private static BookkeeperManager manager = BookkeeperManager.Instance;

		private Button addEventBtn, photoBtn;
		private RadioButton incomeRadio, expenseRadio;
		private EditText dateText, descText, totAmountText;
		private Spinner typeSpinner, accountSpinner, taxSpinner;
		private ImageView photoView;
		private ArrayAdapter adapter;
		private SpinnerAdapter adapter2, adapter3;

		private Entry entry = new Entry();
		private bool isPhotoTaken;

		private AFile picDir; 
		private AFile myDir; 
		private AFile myFile; 
		private AUri myUri;

		private List<Account> income, expense, money;

		protected override void OnCreate (Bundle bundle)
		{
			
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.activity_new_entry);

			income 	= manager.IncomeAccounts;
			expense = manager.ExpenseAccounts;
			money 	= manager.MoneyAccounts;

			addEventBtn 	= FindViewById<Button> 		(Resource.Id.add_event_btn);
			incomeRadio 	= FindViewById<RadioButton> (Resource.Id.income_radio);
			expenseRadio 	= FindViewById<RadioButton> (Resource.Id.expense_radio);

			dateText 		= FindViewById<EditText> 	(Resource.Id.date_editText);
			descText 		= FindViewById<EditText> 	(Resource.Id.desc_editText);
			totAmountText 	= FindViewById<EditText> 	(Resource.Id.amount_editText);

			typeSpinner 	= FindViewById<Spinner> 	(Resource.Id.type_spinner);
			accountSpinner 	= FindViewById<Spinner> 	(Resource.Id.acccount_spinner);
			taxSpinner 		= FindViewById<Spinner> 	(Resource.Id.tax_spinner);

			photoBtn 		= FindViewById<Button> 		(Resource.Id.photo_btn);
			photoView 		= FindViewById<ImageView> 	(Resource.Id.photo_imgView);

			picDir 			= AEnvironment.GetExternalStoragePublicDirectory( AEnvironment.DirectoryPictures); 
			myDir 			= new AFile(picDir, "BookkeeperManager");
			isPhotoTaken 	= false;


			if (!myDir.Exists()) myDir.Mkdirs(); 
		
			adapter = new ArrayAdapter (this, Android.Resource.Layout.SimpleSpinnerItem, manager.TaxRates.Select(tax=>tax.Rate).ToList());
			adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);

			taxSpinner.Adapter = adapter;
			adapter2 = new SpinnerAdapter (this, income);

			typeSpinner.Adapter = adapter2;
			adapter3 = new SpinnerAdapter (this, money);

			accountSpinner.Adapter = adapter3;

			incomeRadio.Checked = true;
				
			incomeRadio.Click += btn_UpdateSpinners;
			expenseRadio.Click += btn_UpdateSpinners;
			accountSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (spinner_AccountNumberSelect);
			typeSpinner.ItemSelected += new EventHandler<AdapterView.ItemSelectedEventArgs> (spinner_TypeSelect);



			addEventBtn.Click += delegate 
			{

				try {
					entry.Date = dateText.Text;
					entry.Description = descText.Text;
					entry.TotalAmount = Convert.ToDouble(totAmountText.Text); 
					entry.TaxRate = taxSpinner.SelectedItemPosition+1;
					if(isPhotoTaken){
						entry.ImageFilePath = myFile.ToString();
					}
					manager.AddEntry(entry);
					Intent i = new Intent(this, typeof(MainActivity));
					i.SetFlags(ActivityFlags.ClearTop);
					StartActivity(i);
				}

				catch {
					Toast.MakeText(this, "Du har glömt att fylla i ett fält...någonstans", ToastLength.Long).Show();
					
				}
					
			};

			photoBtn.Click += delegate
			{
				int photoID = manager.Entries.Count;
				Console.WriteLine("Photo ID: "+photoID);

				myFile = new AFile(myDir, "minFil"+photoID+".jpg");
				myUri = AUri.FromFile(myFile);

				Intent intent = new Intent(MediaStore.ActionImageCapture); 
				intent.PutExtra(MediaStore.ExtraOutput, myUri);

				StartActivityForResult(intent, 3); // 2nd arg is unique id request code

			};
				
		}


		private void spinner_AccountNumberSelect(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			entry.Account = money [e.Position].Number;
			Console.WriteLine("Event Args: "+e);
			Console.WriteLine("Entry Account: "+entry.Account+ "Type: "+entry.Type);
		}
		private void spinner_TypeSelect(object sender, AdapterView.ItemSelectedEventArgs e)
		{
			// Income
			if (incomeRadio.Checked) {
				entry.Type = income [e.Position].Number;
				Console.WriteLine("Event Args: "+e);
				Console.WriteLine("Entry Account: "+entry.Account+" Type: "+entry.Type);
			}
			// Expense
			else
			{
				entry.Type = expense [e.Position].Number;
				Console.WriteLine("Event Args: "+e);
				Console.WriteLine("Entry Account: "+entry.Account+" Type: "+entry.Type);
			}
		}

		private void btn_UpdateSpinners(object sender, EventArgs e) 
		{
			if (incomeRadio.Checked) 
			{
				adapter2 = new SpinnerAdapter (this, income);
				typeSpinner.Adapter = adapter2;
				Console.WriteLine("incomeRadio Adapter2: "+adapter2);

			}
			else
			{
				adapter2 = new SpinnerAdapter (this, expense);
				typeSpinner.Adapter = adapter2;
				Console.WriteLine("expenseRadio Adapter2: "+adapter2);
			}
		}

		//Call back to check what I got
		protected override void OnActivityResult (int requestCode, Result resultCode, Intent data)
		{    
			if (requestCode == 3) { // same as my request code?
				if (resultCode == Result.Ok) {
					photoView.SetAdjustViewBounds (true);
					int height = Resources.DisplayMetrics.HeightPixels;   
					int width = photoView.Width;  

					Bitmap bitmap = ImageUtils.LoadAndScaleBitmap (myUri.Path, width, height);   
					photoView.SetImageBitmap (bitmap);
					isPhotoTaken = true;
				}
			} else {   
				base.OnActivityResult (requestCode, resultCode, data);
				Console.WriteLine ("Oh nooos!"+requestCode+" "+resultCode);
			}
		}

	}
}
