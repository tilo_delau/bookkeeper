﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Bookkeeper
{
	[Activity (Label = "EntryListActivity")]			
	public class EntryListActivity : Activity
	{
		private static BookkeeperManager manager = BookkeeperManager.Instance;
		private List<Entry> entries = manager.Entries;
		private ListView list;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.activity_entry_list);
			list = FindViewById<ListView> (Resource.Id.entry_listView);
			list.Adapter = new EntryListAdapter(this, entries);
			list.ItemClick += delegate(object sender, AdapterView.ItemClickEventArgs e) {
				Intent show = new Intent(this, typeof (EntryItemActivity));
				show.PutExtra("entry", entries[e.Position].Id);
				StartActivity(show);
				
			};


		}
	}
}

