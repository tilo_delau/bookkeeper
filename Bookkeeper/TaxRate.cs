﻿using System;
using SQLite;

namespace Bookkeeper
{
	public class TaxRate
	{
		public TaxRate() { }

		[PrimaryKey, AutoIncrement]
		public int Id { get; private set;}
		public double Rate	{ get; set;	}
	}
}

