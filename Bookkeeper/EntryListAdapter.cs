﻿using System;
using Android.Widget;
using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Content;

namespace Bookkeeper
{
	public class EntryListAdapter : BaseAdapter 
	{ 
		private Activity context;
		private List<Entry> entries;

		public EntryListAdapter (Activity activity, List<Entry> entries)
		{
			this.context = activity;
			this.entries = entries;
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return new JavaObjectWrapper(){ obj = entries[position] }; 
		}

		public override long GetItemId (int position)
		{
			return position;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View view = convertView ?? context.LayoutInflater.Inflate (Resource.Layout.entrylist_item_layout, parent, false);
			view.FindViewById<TextView> (Resource.Id.date_text).Text = entries [position].Date;
			view.FindViewById<TextView> (Resource.Id.description_text).Text = entries [position].Description;
			view.FindViewById<TextView> (Resource.Id.amount_text).Text = entries [position].TotalAmount +"";

			return view; 

		}
		public override int Count
		{
			get 
			{
				return entries.Count;
			}
		}
	
	}
}

